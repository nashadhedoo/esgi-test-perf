import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  stages: [
    { duration: '20s', target: 30 }, // ramp up to 30 users
    { duration: '2m', target: 30 }, // stay at 30 for 2m
    { duration: '20s', target: 0 }, // scale down. (optional)
  ],
};

const API_BASE_URL = 'http://nginx:80/api/chats';

export default function () {
  http.batch([
    ['GET', `${API_BASE_URL}/1`],
    ['GET', `${API_BASE_URL}/simpleList`],
    ['GET', `${API_BASE_URL}/dataTestList`],
    ['GET', `${API_BASE_URL}/grandeList`],
  ]);

  sleep(1);
}
