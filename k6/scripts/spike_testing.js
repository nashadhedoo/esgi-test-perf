import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  stages: [
    { duration: '10s', target: 10 }, // below normal load
    { duration: '20s', target: 20 },
    { duration: '10s', target: 100 }, // spike to 100 users
    { duration: '60s', target: 100 }, // stay at 100 for 60s
    { duration: '10s', target: 20 }, // scale down. Recovery stage.
    { duration: '20s', target: 10 },
    { duration: '10s', target: 0 },
  ],
};
export default function () {
  const BASE_URL = 'http://nginx:80/api/chats';

  let responses = http.batch([
    [
      'GET',
      `${BASE_URL}/1`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/simpleList`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/dataTestList`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/grandeList`,
      null,
      null,
    ],
  ]);

  sleep(1);
}
