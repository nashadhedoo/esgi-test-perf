import http from 'k6/http';
import {check, sleep} from 'k6';

export let options = {
  vus: 1,
  duration: '1m',

  thresholds: {
    http_req_duration: ['p(99)<1500'], // 99% of requests must complete below 1.5s
  },
};

export default () => {
  let response = http.get(`http://nginx:80/api/chats/grandeList`, {headers: {Accepts: "application/json"}});
  check(response, {"status is 200": (r) => r.status === 200});
  sleep(1);
};
