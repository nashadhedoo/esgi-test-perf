import http from 'k6/http';
import {check, sleep} from "k6";

export let options = {
  stages: [
    // Ramp-up from 1 to 100 virtual users (VUs) in 10s
    {duration: "10s", target: 100},

    // Stay at rest on 100 VUs for 10s
    {duration: "30s", target: 100},

    // Ramp-down from 100 to 0 VUs for 10s
    {duration: "10s", target: 0}
  ]
};

export default function () {
  const response = http.get("http://nginx:80/api/chats/1", {headers: {Accepts: "application/json"}});
  check(response, {"status is 200": (r) => r.status === 200});
  sleep(.300);
};
