Projet de test Performance.

## Installation avec docker

- docker-compose build --no-cache
- docker-compose up -d 

Pour Windows : 
```
$ cd docker/nginx/
$ find . -name "*.sh" | xargs dos2unix
```

##Installation des assets
```
$ docker-compose up node
```

## Doctrine
#####Mise à jour de votre BDD
```
docker-compose exec php bin/console doctrine:schema:update --force
```
#####Custom query avec DQL (repository)
https://symfony.com/doc/current/doctrine.html#querying-with-the-query-builder
https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/query-builder.html

#####MAJ BDD et Migration
https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
```
docker-compose exec php bin/console doctrine:schema:update --dump-sql
docker-compose exec php bin/console doctrine:schema:update --force
docker-compose exec php bin/console make:migration
```
## Import csv massiv
Importer les données du csv sur adminer, mettez :

```
    copy chats(
    id,
    url,
    age,
    gender,
    size,
    coat,
    breed
    )
    FROM '/chats-profiles.csv'
    DELIMITER ','
    CSV HEADER;
```