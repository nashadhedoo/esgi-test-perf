<?php

namespace App\Controller;

use App\Repository\ChatsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("api/chats")
 */
class ChatsController extends AbstractController
{

    /**
     * @Route("/dataTestList", name="chats_dataTestList", methods={"GET"})
     */
    public function dataTestList( ): Response
    {
        $listData = array();
        for($i = 0; $i <= 60; $i++)
        {
                        $data = "data test 1 ".$i;
                        $data .= ":data test 4".rand(0,20);
                        array_push($listData,$data);
        }
        return new Response(json_encode($listData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }
     /**
     * @Route("/simpleList", name="chats_simpleList", methods={"GET"})
     */
    public function simpleList(ChatsRepository $chatsRepository): Response
    {
        $datas = $chatsRepository->findSimpleList(500);
        $listData = array();
        foreach ($datas as $data)
        {
            array_push($listData,$data->toArray());
        }
        return new Response(json_encode($listData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }


    /**
     * @Route("/grandeList", name="chats_grandeList", methods={"GET"})
     */
    public function grandeList(ChatsRepository $chatsRepository): Response
    {

        $datas = $chatsRepository->findGrandeList(20000);
        $listData = array();
        foreach ($datas as $data)
        {
            array_push($listData,$data->toArray());
        }
        return new Response(json_encode($listData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }


        /**
     * @Route("/selectList", name="chats_selectList", methods={"GET"})
     */
    /*public function selectList(ChatsRepository $chatsRepository): Response
    {

        // A faire
    }*/


}
