import {createContext} from "react";

export const IndicateurContext = createContext();

export function IndicateurProvider({children}) {
  const url = `http://localhost:8083/api/chats`;
  const options = {
    mode: "cors",
    method: "GET",
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": '*',
    }
  };

  const last = async () => {
    return await fetch(url + "/last", {
      ...options,
    }).then(res => res.json());
  };

  const big = async () => {
    return await fetch(url + "/big", {
      ...options,
    }).then(res => res.json());
  };

  const low = async () => {
    return await fetch(url + "/low", {
      ...options,
    }).then(res => res.json());
  };

  const findOne = async (id) => {
    return await fetch(url + `/${id}`, {
      ...options,
    }).then(res => res.json());
  }

  const deleteOne = async (id) => {
    await fetch(url + `/${id}`, {
      ...options,
      method: "DELETE"
    });
  };

  const login = ({username, password}) => {
    return fetch("http://localhost:8083/login", {
      ...options,
      method: "POST",
      body: JSON.stringify({username, password})
    }).then(res => res.json())
      .then(data => {
        if (data) localStorage.setItem('token', data);
      })
      .catch(e => console.error(e));
  };

  const logout = () => {
    localStorage.removeItem('token');
  };

  const createOne = async (indicateur) => {
    return await fetch(url, {
      ...options,
      method: "POST",
      body: JSON.stringify({...indicateur}),
    }).then(res => res.json())
      .catch(e => console.error(e));
  };

  return (
    <IndicateurContext.Provider value={{last, big, low, findOne, deleteOne, login, logout, createOne}}>
      {children}
    </IndicateurContext.Provider>
  );
}
