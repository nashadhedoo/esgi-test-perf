import React, {useContext, useEffect, useState} from "react";
import {IndicateurContext} from "../../contexts";

export function Big({}) {
  const [list, setList] = useState([]);
  const {big} = useContext(IndicateurContext);

  useEffect(async () => setList(await big()), []);

  return (
    <div>
      {list && (
        <div>
          <ul>
            {list.map((elem) => (
              <li key={elem.id}>
                id: {elem.id}<br/>
                url: {elem.url}<br/>
                age: {elem.age}<br/>
                gender: {elem.gender}<br/>
                size: {elem.size}<br/>
                coat: {elem.coat}<br/>
                breed: {elem.breed}<br/>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
