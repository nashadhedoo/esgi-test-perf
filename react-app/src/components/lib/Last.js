import React, {useContext, useEffect, useState} from "react";
import {IndicateurContext} from "../../contexts";

export function Last({}) {
  const [list, setList] = useState([]);
  const {last} = useContext(IndicateurContext);

  useEffect(async () => setList(await last()), []);

  return (
    <div>
      {list && (
        <div>
          <ul>
            {list.map((elem) => (
              <li key={elem.id}>
                id: {elem.id}<br/>
                url: {elem.url}<br/>
                age: {elem.age}<br/>
                gender: {elem.gender}<br/>
                size: {elem.size}<br/>
                coat: {elem.coat}<br/>
                breed: {elem.breed}<br/>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
