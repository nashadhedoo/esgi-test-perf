import React, {useContext, useState} from "react";
import {IndicateurContext} from "../../contexts";

export function Create({}) {
  const {createOne} = useContext(IndicateurContext);
  const [indicateur, setIndicateur] = useState({
    url: "",
    age: "",
    gender: "",
    size: "",
    coat: "",
    breed: "",
  });


  const handleChange = (event) => {
    setIndicateur({
      ...indicateur,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = () => {
    createOne(indicateur);
  }

  return (
    <div>
      <label>url</label><br/>
      <input type={"text"} value={indicateur.url} name={"url"} onChange={handleChange}/><br/>
      <label>age</label><br/>
      <input type={"text"} value={indicateur.age} name={"age"} onChange={handleChange}/><br/>
      <label>gender</label><br/>
      <input type={"text"} value={indicateur.gender} name={"gender"} onChange={handleChange}/><br/>
      <label>size</label><br/>
      <input type={"text"} value={indicateur.size} name={"size"} onChange={handleChange}/><br/>
      <label>coat</label><br/>
      <input type={"text"} value={indicateur.coat} name={"coat"} onChange={handleChange}/><br/>
      <label>breed</label><br/>
      <input type={"text"} value={indicateur.breed} name={"breed"} onChange={handleChange}/><br/>
      <button onClick={handleSubmit}>Créer une entrée</button>
    </div>
  );
}
