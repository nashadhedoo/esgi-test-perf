import React, {useContext, useEffect, useState} from "react";
import {useParams} from "react-router";
import {IndicateurContext} from "../../contexts";

export function FindOne({}) {
  const {id} = useParams()
  const [list, setList] = useState([]);
  const {findOne, deleteOne} = useContext(IndicateurContext);

  useEffect(async () => setList(await findOne(id)), []);

  return (
    <div>
      id: {list.id}<br/>
      url: {list.url}<br/>
      age: {list.age}<br/>
      gender: {list.gender}<br/>
      size: {list.size}<br/>
      coat: {list.coat}<br/>
      breed: {list.breed}<br/>
      <a onClick={async () => {deleteOne(list.id)}}>Delete</a>
    </div>
  );
}
