import React, {useContext} from "react";
import { Link } from "react-router-dom";
import {IndicateurContext} from "../contexts";
import { Button } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';


export function Header() {
  const token = localStorage.getItem("token");
  const {logout} = useContext(IndicateurContext)
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
     <div>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        Open Menu
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem to="/low" onClick={handleClose}>data 1</MenuItem>
        <MenuItem to="/big" onClick={handleClose}>data 2</MenuItem>
        <MenuItem to="/last" onClick={handleClose}>data 3</MenuItem>
        <MenuItem to="/new" onClick={handleClose}>nouvelle donnée</MenuItem>
        <MenuItem to="/login" onClick={handleClose}>connexion</MenuItem>
        <MenuItem onClick={logout}>Deconnexion</MenuItem>
      </Menu>
    </div>

    
  );
}
