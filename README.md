# Projet teste de performance

- Lancer le docker compose
  ```docker-compose up -d```


- Pour lancer les testes k6, il faut aller dans le dossier k6 se trouvant à la racine du projet. Modifier dans le
  dossier scripts le script à lancer (url, stage de test, etc...) puis, dans le fichier ````run-load-test.sh````,
  dé-commenté le teste qu'on souhaite lancer puis exécuter le script.
  
